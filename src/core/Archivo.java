/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Producto;
import views.MainForm;
/**
 *
 * @author Sistemas-14
 */
public class Archivo {
    File archivo;
    
    public Archivo(){
    }
    
    public void createFile(){
        archivo = new File("Nuevo_archivo.txt");
        
        try {
            if(archivo.createNewFile()){
                //Archivo creado
            }
        } catch (IOException ex) {
            System.err.println("Error: "+ex);
        }
    }
    
    public void writeFile(Producto producto){
        try {
            FileWriter escribir = new FileWriter(archivo,true);
            escribir.write("\r"+producto.getId()+"%"+producto.getNombre()+"%"+producto.getPrice());
            escribir.close();
        } catch (IOException ex) {
            System.err.println("Error: "+ex);
        }
    }
}
